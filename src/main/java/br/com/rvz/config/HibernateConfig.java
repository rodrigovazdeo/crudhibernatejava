package br.com.rvz.config;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HibernateConfig {
	
	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;
	private static final Logger LOG = LogManager.getLogger(HibernateConfig.class.getName());
	
	public EntityManager build() {
		LOG.info("Configurando a fábrica de entidades do hibernate");
		this.entityManagerFactory = Persistence.createEntityManagerFactory("ApplicationDatabaseHibernateNewsTech");
		this.entityManager = this.entityManagerFactory.createEntityManager();
		return this.entityManager;
	}

	public EntityManager getEntityManager() {
		LOG.info("Retornando a instancia do gerenciador de entidades");
		return entityManager;
	}
	
	public void closeEntityManager() {
		LOG.info("Fechando o gerenciador de entidades");
		if (this.entityManager.isOpen()) {
			this.entityManager.clear();
		}
	}
	
	public void closeEntityManagerFactory() {
		LOG.info("Fechando o gerenciador de fábrica de entidades");
		if (this.entityManagerFactory.isOpen()) {
			this.entityManagerFactory.close();
		}
	}
}