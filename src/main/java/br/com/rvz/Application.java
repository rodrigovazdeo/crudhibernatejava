package br.com.rvz;

import br.com.rvz.config.HibernateConfig;

public class Application {
	public static void main(String[] args) {
		HibernateConfig hibernateConfig = new HibernateConfig();
		hibernateConfig.build();
		hibernateConfig.closeEntityManager();
		hibernateConfig.closeEntityManagerFactory();
	}
}
